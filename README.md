# http_ws - C++ based websocket server framework using libwebsockets on Windows OS

##Goals:
* Server has to accept **HTTP UPGRADE** requests to path __/ws__, websockets protocol **version >=13**.
* Server responds to any **text messages** with an echo where **question marks** are **replaced** with **exclamation marks**.
* If server receives a **binary message**, the **connection is closed**.

####Project hours
Project started Nov 3, 2018 and finished Nov 18, 2018
Total hours: __~33__

##Design choices:
**libwebsockets** was chosen as the only framework the project relies upon (including a couple of its own dependencies).
This simple C based websocket framework is very easy to start a project with. At the beginning, a couple of other
C++ based websocket frameworks were tested, namely **µWebSockets**, **websocket++**, **seasocks**, which proved to be unsuitable for such a small project.

**µWebSockets** is a tiny C++ based framework. Its downsides are a lot of dependencies (particularly on Boost). While it offers an inovative and clean solution
to configure server logic, setting it up took too much time and was discarded.

**websocket++** while being header-only also suffers from having too many dependencies. It is a medium sized C++ framework, and while including it into the project is easy,
its dependencies (particularly Boost (again)) need too much precompiler flags to configure.

**seasocks** this framework is very similar to **libwebsockets** with its size, usability and dependencies (none), currently it is available only for Linux.

Having a C based library might be problematic when using it in C++ code.
However, some solutions might be used to alleviate this problem:

* Using C-like code (e.g. the testing framework)
* Using design patterns, such as singletons and delegation patterns.

There may be many more ways to reduce the friction between C and C++ code, but this project was too small to use them.

###Singleton pattern:
C callback functions provided in libwebsockets are not object oriented (e.g. they provide no intrinsic way to reference the responsible object)
While some C libraries provide a way to pass in a pointer to the object (libwebsockets also), they might be convoluted (casting void pointers - bleurgh) or limiting (creating a struct of pointers
to get all the necessary data - it might grow every time anything new is needed)
Singletons solve this problem by having only one object that can be referenced while still being compatible with OOP approach.
They also suggest to the user of this framework, that the object is intended to carry a lot of workload (e.g. one server object per application) or separate instances of it would not prove to be useful.
While a way is provided for the user to write his own server implementation using WsBase, the provided server class is nudging in the direction of extending it instead.
Some other utility classes could have used the singleton pattern (i.e. logger, persistence etc).
###Delegation pattern:
Delegation pattern provides a comfortable way to implement callbacks in C++, and builds on the more traditional C-like function pointer passing.
Delegates can be registered, unregistered or changed at runtime, which allows a flexible way to have an event system, while also logically connecting related callback functions.
###C code
When everything else fails, falling back to C code is also an option. Even then, it has more features than pure C (e.g. namespaces, with which some amount of code encapsulation might be 
achieved).
In a lot of cases, pure C code can be a lot more performant: modern CPU's are not built with OOP in mind, so having logic tied with actual data leads to cache misses etc.
While C is not safe from these issues, it is a lot harder to screw up, and since it is mostly used on a very low level as a third-party dependency it should be performant enough for any application.

##libwebsockets:
libwebsockets is very flexible - it provides many ways to take control of the logic and data flow, and, while also having its default methods, should the user not require any advanced configuration,
is easy to use. A server, client or any other websocket solution could be developed in two methods - one for initialization and running the event loop, the other for event logic.

##Testing framework:
The project provides its own proprietary testing framework. While it could be improved, its design goal was to provide an easy and a fast way to include new tests. Tests should never impede development
progress, be hard to fix or to replace. 
While the tests provided are integration tests only, should a similar project grow in size, unit tests would also be necessary. This project, however, is not suitable for unit tests - a lot more
unnecessary abstraction would be needed to test each component seperately. Here, the classes are too primitive to be tested in such a way.
The project was not developed using TTD, so the provided tests are only for regression testing.
The tests (including the server) run in a single thread and on a single loop - while this is not ideal, it provides a very easy and synchronous way to debug every piece of code.

#Ideas for the future:
Since the project relies only on a C based multi-platform websockets library, provided the correct project files for the system, it can easily be ported.
More testing methods, should the project grow in size. Currently, an asynchronous RWT could be implemented without much hassle, just have separate applications for each test.
Logger, persistence and other useful features could be easily added to store session data, configuration and other must-have files.
A threading framework could be added, to provide an easy way to have multithreaded applications using this framework.
