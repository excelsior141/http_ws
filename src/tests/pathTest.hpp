#pragma once

#include <libwebsockets.h>

#include "utility/utility.hpp"

namespace PathTest
{
	bool finished = false;
	bool success = false;

	int callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len)
	{
		switch (reason)
		{
			case LWS_CALLBACK_CLIENT_ESTABLISHED:
			case LWS_CALLBACK_CLIENT_WRITEABLE:
			{
				finished = true; // Fail condition
			} break;
			case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
			{
				finished = true;
				success = true;
				return 1;
			}
			case LWS_CALLBACK_CLIENT_CLOSED:
			{
				finished = true;
				return 1;
			} break;
		}

		return 0;
	}

	lws_protocols protocols[] =
	{
		{ "ws", callback, 0, 0 }, // Unsupported protocol
		{ NULL, NULL, 0, 0 }
	};
}
