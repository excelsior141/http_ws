#pragma once

#include <libwebsockets.h>

#include "utility/utility.hpp"

namespace BinaryMessageTest
{
	bool finished = false;
	bool success = false;

	bool ignoreClose = false;

	const char* sendText = "This is a binary message\n";

	int callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len)
	{
		switch (reason)
		{
			case LWS_CALLBACK_CLIENT_ESTABLISHED:
			{
				lws_callback_on_writable(wsi);
			} break;
			case LWS_CALLBACK_CLIENT_WRITEABLE:
			{
				size_t text_length = strlen(sendText);

				unsigned char* payload = static_cast<unsigned char*>(malloc(LWS_PRE + text_length));
				memset(payload, 0, LWS_PRE + text_length);

				if (Utility::assertNot(payload == nullptr, "Unable to allocate payload for the message: Out of memory"))
					return 1;

				sprintf((char*)payload + LWS_PRE, sendText);

				lws_write(wsi, payload + LWS_PRE, text_length, LWS_WRITE_BINARY);
			} break;
			case LWS_CALLBACK_CLIENT_RECEIVE:
			{
				finished = true;
				ignoreClose = true;

				return 1;
			}
			case LWS_CALLBACK_CLIENT_CLOSED:
			{
				if (ignoreClose == false)
				{
					success = true;
					finished = true;
				}
			} break;
		}

		return 0;
	}

	lws_protocols protocols[] =
	{
		{ "ws", callback, 0, 0 },
		{ NULL, NULL, 0, 0 }
	};
}
