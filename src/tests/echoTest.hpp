#pragma once

#include <libwebsockets.h>

#include "utility/utility.hpp"

namespace EchoTest
{
	bool finished = false;
	bool success = false;

	const char* sendText = "Testing:!?!?\n";
	const char* receiveText = "Testing:!!!!\n";

	int callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len)
	{
		switch (reason)
		{
			case LWS_CALLBACK_CLIENT_ESTABLISHED:
			{
				lws_callback_on_writable(wsi);
			} break;
			case LWS_CALLBACK_CLIENT_WRITEABLE:
			{
				size_t text_length = strlen(sendText);

				unsigned char* payload = static_cast<unsigned char*>(malloc(LWS_PRE + text_length));
				memset(payload, 0, LWS_PRE + text_length);

				if (Utility::assertNot(payload == nullptr, "Unable to allocate payload for the message: Out of memory"))
					return 1;

				sprintf((char*)payload + LWS_PRE, sendText);

				lws_write(wsi, payload + LWS_PRE, text_length, LWS_WRITE_TEXT);
			} break;
			case LWS_CALLBACK_CLIENT_RECEIVE:
			{
				char* message = new char[len + 1];
				memset(message, 0, len + 1);
				memcpy(message, in, len + 1);

				if (Utility::assertNot(strcmp(receiveText, message) != 0, "Echo test failed - received message does not match expected string"))
					return 1;

				success = true;

				return 1;
			} break;
			case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: // Fallthrough
			case LWS_CALLBACK_CLIENT_CLOSED:
			{
				finished = true;
			} break;
		}

		return 0;
	}

	lws_protocols protocols[] =
	{
		{ "ws", callback, 0, 0 },
		{ NULL, NULL, 0, 0 }
	};
}
