#include <libwebsockets.h>

#include "utility/utility.hpp"
#include "common/WsBase.hpp"

bool g_stop = false;

static int callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len)
{
	switch (reason)
	{
		case LWS_CALLBACK_CLIENT_ESTABLISHED:
		{
			lws_callback_on_writable(wsi);
		} break;
		case LWS_CALLBACK_CLIENT_WRITEABLE:
		{
			const char* text = "Testing:!?!?\n";
			size_t text_length = strlen(text);

			unsigned char* payload = static_cast<unsigned char*>(malloc(LWS_PRE + text_length));
			memset(payload, 0, LWS_PRE + text_length);

			if (Utility::assertNot(payload == nullptr, "Unable to allocate payload for the message: Out of memory"))
				return 1;

			sprintf((char*)payload + LWS_PRE, text);

			lws_write(wsi, payload + LWS_PRE, text_length, LWS_WRITE_TEXT);

			std::cout << "Message sent: " << text << std::endl;
		} break;
		case LWS_CALLBACK_CLIENT_RECEIVE:
		{
			char* message = new char[len + 1];
			memset(message, 0, len);
			memcpy(message, in, len);

			std::cout << "Message received: " << message << std::endl;

			return 1;
		} break;
		case LWS_CALLBACK_CLIENT_CLOSED:
		{
			g_stop = true;
		} break;
	}

	return 0;
}

lws_protocols protocols[] =
{
	{ "ws", callback, 0, 0 },
	{ NULL, NULL, 0, 0 }
};

int main(int argc, const char** argv)
{
	lws_set_log_level(lws_log_levels::LLL_USER, nullptr);

	lws_client_connect_info* connect_info = new lws_client_connect_info;
	lws_context_creation_info* creation_info = new lws_context_creation_info;

	memset(connect_info, 0, sizeof(lws_client_connect_info));
	memset(creation_info, 0, sizeof(lws_context_creation_info));

	creation_info->port = CONTEXT_PORT_NO_LISTEN;
	creation_info->options = LWS_SERVER_OPTION_VALIDATE_UTF8;
	creation_info->protocols = protocols;

	connect_info->port = 8099;
	connect_info->address = "localhost";
	connect_info->path = "/ws";
	connect_info->host = connect_info->address;
	connect_info->origin = connect_info->address;
	connect_info->protocol = protocols[0].name;

	WsBase client;

	if (Utility::assertNot(client.init(creation_info) == false, "Client is unable to initiate"))
		return -1;

	if (Utility::assertNot(client.connect(connect_info) == false, "Client is unable to connect"))
		return -1;

	while (!g_stop)
		client.update(0);

	client.cleanup();

	return 0;
}