#pragma once

#include "common/wsBase.hpp"
#include "utility/singleton.hpp"

struct lws_protocols;

class Server : public Singleton<Server>, public WsBase
{
	enum Protocols
	{
		WS = 0,
		TERMINATOR,
		COUNT
	};

public:

	// Start listening for coming connections on a port
	bool listen(int port);

	// Server has initialized and is ready to accept connections
	bool isRunning() { return m_running; }

private:

	// Hide parent function from access
	bool init(lws_context_creation_info* creation_info) override final { return __super::init(creation_info); };

protected:

	// Hiding constructor and allowing only base singleton class to access it
	friend Singleton<Server>;
	explicit Server();

private:

	// Deleting copy and assignment constructors
	Server(const Server& other) = delete;
	Server& operator=(const Server& other) = delete;

private:

	static int callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len);

private:

	int m_port;
	bool m_running;
	std::string m_path;
	lws_protocols m_protocols[Protocols::COUNT];
};