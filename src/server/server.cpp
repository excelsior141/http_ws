#include <libwebsockets.h>

#include <vector>

#include "utility/utility.hpp"
#include "server.hpp"

Server::Server()
	: m_path("/ws")
	, m_running(false)
{
	m_protocols[Protocols::WS] =			{ "ws", callback, 0, 0 };
	m_protocols[Protocols::TERMINATOR] =	{ NULL, NULL, 0, 0 }; // terminator - required by libwebsockets
}

bool Server::listen(int port)
{
	if (Utility::assertNot(port == 0 || port > 65535, "Invalid port specified - please specify an integer [1-65535]"))
		return false;

	m_creation_info = new lws_context_creation_info;
	memset(m_creation_info, 0, sizeof(lws_context_creation_info));

	m_creation_info->port = port;
	m_creation_info->pt_serv_buf_size = 32 * 1024;
	m_creation_info->options = LWS_SERVER_OPTION_VALIDATE_UTF8;
	m_creation_info->protocols = m_protocols;

	return init(m_creation_info);
}

int Server::callback(lws* wsi, lws_callback_reasons reason, void* user, void* in, size_t len)
{
	switch (reason)
	{
		case LWS_CALLBACK_PROTOCOL_INIT:
		{
			Server::singleton()->m_running = true;
		} break;
		case LWS_CALLBACK_WSI_DESTROY:
		case LWS_CALLBACK_PROTOCOL_DESTROY:
		{
			Server::singleton()->m_running = false;
		} break;
		case LWS_CALLBACK_FILTER_PROTOCOL_CONNECTION:
		{
			auto isInvalid =
			[](lws* wsi, lws_token_indexes token, int fragment, auto predicate) -> bool
			{
				bool result = true;
				int len = lws_hdr_fragment_length(wsi, token, 0);

				if (len > 0)
				{
					len += 1; // Additional space for null terminator - required by libwebsockets

					char* buffer = new char[len];
					memset(buffer, 0, len);

					lws_hdr_copy_fragment(wsi, buffer, len, token, fragment);
					result = predicate(buffer);

					delete[] buffer;
				}

				return result;
			};

			if (Utility::assertNot(isInvalid(wsi, WSI_TOKEN_GET_URI, 0,
				[](char* fragment_value) -> bool 
				{ 
					return (strcmp(fragment_value, Server::singleton()->m_path.c_str()) != 0);
				}
			), "Client disconnected: Invalid path"))
				return 1;

			if (Utility::assertNot(isInvalid(wsi, WSI_TOKEN_VERSION, 0,
				[](char* fragment_value) -> bool
				{
					return atoi(fragment_value) < 13;
				}
			), "Client disconnected: Unsupported websocket protocol version"))
				return 1;

		} break;
		case LWS_CALLBACK_RECEIVE:
		{
			if (Utility::assertNot(lws_frame_is_binary(wsi) == 1, "Binary frame received. Closing connection"))
				return 1;

			char* message = static_cast<char*>(in);

			for (char* ptr = message; ptr < message + len; ++ptr)
				if (*ptr == '?') *ptr = '!';

			lws_write(wsi, static_cast<unsigned char*>(in), len, LWS_WRITE_TEXT);
		} break;
	}

	return 0;
}