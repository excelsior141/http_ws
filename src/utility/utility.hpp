#pragma once

#include <string>
#include <iostream>

namespace Utility
{
	static bool assertNot(bool condition, const std::string& message)
	{
		if (condition)
			std::cout << message << std::endl;

		return condition;
	}
}
