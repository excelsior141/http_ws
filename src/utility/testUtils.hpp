#pragma once

#include "common/wsBase.hpp"

static lws_client_connect_info* createConnectInfo(int port, const char* address, const char* path, const char* protocolName)
{
	lws_client_connect_info* connect_info = new lws_client_connect_info;

	memset(connect_info, 0, sizeof(lws_client_connect_info));

	connect_info->port = port;
	connect_info->address = address;
	connect_info->path = path;
	connect_info->host = connect_info->address;
	connect_info->origin = connect_info->address;
	connect_info->protocol = protocolName;

	return connect_info;
}

static lws_context_creation_info* createCreationInfo(lws_protocols* protocols)
{
	lws_context_creation_info* creation_info = new lws_context_creation_info;

	memset(creation_info, 0, sizeof(lws_context_creation_info));

	creation_info->port = CONTEXT_PORT_NO_LISTEN;
	creation_info->options = LWS_SERVER_OPTION_VALIDATE_UTF8;
	creation_info->protocols = protocols;

	return creation_info;
}

struct Test
{
	const char* name;
	WsBase* socket;
	bool* success;
	bool* finished;
};

static std::vector<Test> tests;

static bool testsFinished()
{
	bool finished = true;

	for (const Test& test : tests)
	{
		if (*test.finished == false)
		{
			finished = false;
			break;
		}
	}

	return finished;
}

static bool registerTest(const char* name, const char* address, const char* path, int port, lws_protocols* protocols, bool* finished, bool* success)
{
	WsBase* socket = new WsBase();

	if (Utility::assertNot(socket->init(createCreationInfo(protocols)) == false, std::string(name) + std::string(" is unable to initiate")))
		return false;

	if (Utility::assertNot(socket->connect(createConnectInfo(port, address, path, protocols[0].name)) == false, std::string(name) + std::string(" is unable to connect")))
		return false;

	Test test
	{
		name,
		socket,
		success,
		finished
	};

	tests.push_back(test);

	return true;
}

static bool outOfTime(time_t startTime, int timeout)
{
	return (time(nullptr) - startTime > timeout);
}
