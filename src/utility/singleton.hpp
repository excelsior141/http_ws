#pragma once

template <typename T>
class Singleton
{
public:

	// No need to explain this
	static T* singleton()
	{
		if (m_instance == nullptr)
			m_instance = new T();

		return m_instance;
	}

protected:

	// Hiding default constructor, so only the children can access
	explicit Singleton<T>() {};

private:

	// Deleting copy and assignment constructors
	Singleton<T>(const Singleton<T>& other) = delete;
	Singleton<T>& operator=(const Singleton<T>& other) = delete;	

private:

	inline static T* m_instance = nullptr;

};
