#pragma once

struct lws_context_creation_info;
struct lws_client_connect_info;
struct lws_context;
struct lws;

class WsBase
{
public:

	class Delegate
	{
	public:
		virtual void onContextDestroyed() = 0;
	};

	WsBase();
	virtual ~WsBase();

	void setDelegate(Delegate* delegate) { m_delegate = delegate; }

	// Initialize context with given information
	virtual bool init(lws_context_creation_info* creation_info);

	// Connect to another websocket with given information
	virtual bool connect(lws_client_connect_info* connect_info);

	// Destroy the session and free associated resources
	virtual void cleanup();

	// Loop through the event stack
	virtual bool update(int timeout);

protected:

	Delegate* m_delegate;

	lws_context_creation_info* m_creation_info;
	lws_client_connect_info* m_connect_info;
	lws_context* m_context;
	lws* m_connection;
};