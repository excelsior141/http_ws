#include <libwebsockets.h>

#include "utility/utility.hpp"
#include "wsBase.hpp"

WsBase::WsBase()
	: m_creation_info(nullptr)
	, m_connect_info(nullptr)
	, m_context(nullptr)
	, m_connection(nullptr)
{
}

WsBase::~WsBase()
{
	cleanup();

	delete m_creation_info;
}

bool WsBase::init(lws_context_creation_info* creation_info)
{
	if (Utility::assertNot(creation_info == nullptr, "Creation info was not provided"))
		return false;

	m_creation_info = creation_info;

	cleanup();

	m_context = lws_create_context(m_creation_info);

	if (Utility::assertNot(m_context == nullptr, "libwebsockets context initialization failed"))
		return false;

	return true;
}

bool WsBase::connect(lws_client_connect_info* connect_info)
{
	if (Utility::assertNot(connect_info == nullptr, "Connect info was not provided"))
		return false;

	m_connect_info = connect_info;

	if (Utility::assertNot(m_context == nullptr, "Websocket was not initiated."))
		return false;

	m_connect_info->context = m_context;
	m_connection = lws_client_connect_via_info(m_connect_info);

	if (Utility::assertNot(m_connection == nullptr, "Unable to connect"))
		return false;

	return true;
}

void WsBase::cleanup()
{
	if (m_context != nullptr)
	{
		if (m_delegate)
			m_delegate->onContextDestroyed();

		lws_context_destroy(m_context);
	}

	if (m_connect_info != nullptr)
	{
		m_connect_info->context = nullptr;
		delete m_connect_info;
	}
	
	m_context = nullptr;
	m_connect_info = nullptr;
	m_connection = nullptr;
}

bool WsBase::update(int timeout)
{
	return (lws_service(m_context, timeout) == 0);
}