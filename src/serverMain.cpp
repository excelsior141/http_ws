#include <libwebsockets.h>

#include "utility/utility.hpp"
#include "server/server.hpp"

int main(int argc, const char** argv)
{
	lws_set_log_level(lws_log_levels::LLL_USER, nullptr);

	std::string port_arg = "-port";
	const char* port_str = nullptr;
	int port = 0;

	port_str = lws_cmdline_option(argc, argv, port_arg.c_str());
	if (Utility::assertNot(port_str == nullptr, std::string("Listening port needs to be specified with an argument:\n" + std::string(argv[0]) + " " + port_arg + " [1-65535]")))
		return -1;

	Server* server = Server::singleton();

	if (Utility::assertNot(server->listen(atoi(port_str)) == false, "Server is unable to start listening"))
		return -1;

	while (true)
		server->update(0);

	return 0;
}