#include <libwebsockets.h>

#include <vector>

#include "utility/utility.hpp"
#include "utility/testUtils.hpp"
#include "common/wsBase.hpp"
#include "server/server.hpp"

#include "tests/echoTest.hpp"
#include "tests/pathTest.hpp"
#include "tests/protocolTest.hpp"
#include "tests/binaryMessageTest.hpp"

int main(int argc, char** argv)
{
	lws_set_log_level(lws_log_levels::LLL_USER, nullptr);

	int port = 8099;
	int timeout = 3;

	time_t server_start_time = time(nullptr);

	Server* server = Server::singleton();
	if (Utility::assertNot(server->listen(port) == false, "Server is unable to start listening"))
		return -1;

	while (server->isRunning() == false && outOfTime(server_start_time, timeout) == false)
		server->update(0);

	if (Utility::assertNot(outOfTime(server_start_time, timeout), "Server is unable to start and has timed out"))
		return -1;

	registerTest("Echo test",				"localhost", "/ws",		port, EchoTest::protocols,				&EchoTest::finished,				&EchoTest::success);
	registerTest("Path test",				"localhost", "/wss",	port, PathTest::protocols,				&PathTest::finished,				&PathTest::success);
	registerTest("Protocol test",			"localhost", "/ws",		port, ProtocolTest::protocols,			&ProtocolTest::finished,			&ProtocolTest::success);
	registerTest("Binary message test",		"localhost", "/ws",		port, BinaryMessageTest::protocols,		&BinaryMessageTest::finished,		&BinaryMessageTest::success);

	time_t test_start_time = time(nullptr);

	while (testsFinished() == false && outOfTime(test_start_time, timeout) == false)
	{
		server->update(0);

		for (const Test& test : tests)
			test.socket->update(0);
	}

	if (Utility::assertNot(outOfTime(test_start_time, timeout), "Tests ran out of time"))
	{
		for (const Test& test : tests)
			std::cout << test.name << ": " << (*test.finished ? "finished" : "timed out") << std::endl;
	}

	std::cout << "Results:" << std::endl;

	for (const Test& test : tests)
		std::cout << test.name << ": " << (*test.success ? "passed" : "failed") << std::endl;

	return 0;
}